#!/usr/bin/python3

import platform
import os
import subprocess
import psutil
from datetime import datetime
# // TODO create graphics in the console
# //TODO get network information
# //TODO more I/O info


def get_size(bytes, suffix="B"):
    factor = 1024
    for unit in ["", "K", "M", "G", "T"]:
        if bytes < factor:
            return f"{bytes:.2f}{unit}{suffix}"
        bytes /= factor


# userID = os.getuid()
# userN = os.getlogin()
# Boot Time
boot_time_timestamp = psutil.boot_time()
# CPU frequencies
cpufq = psutil.cpu_freq()
# Memory details
mem = psutil.virtual_memory()
# SWAP
swap = psutil.swap_memory()
# Partitions
partitions = psutil.disk_partitions()
# I/O
disk_io = psutil.disk_io_counters()

print()
########################################################################################################
# Print OS information
print("="*40, "OS information", "="*40)
print(f'system              :', platform.system())
print(f'node                :', platform.node())
print(f'release             :', platform.release())
print(f'version             :', platform.version())
# print(f'MacOS Version     :', platform.mac_ver())
print(f'machine             :', platform.machine())
print(f'processor           :', platform.processor())
print(f'User                :', os.getlogin())
print(f'userID              :', os.getuid())


# print(f"My user is:", userID, "and my id is:", userN)

# Print Boot Time
print()
print("="*40, "Boot Time", "="*40)
# boot_time_timestamp = psutil.boot_time()
bt = datetime.fromtimestamp(boot_time_timestamp)
print(
    f"Boot Time                  : {bt.year}/{bt.month}/{bt.day} {bt.hour}:{bt.minute}:{bt.second}")


# let's print CPU information
print()
print("="*40, "CPU Info", "="*40)
# number of cores
print("Physical cores                 :", psutil.cpu_count(logical=False))
print("Total cores                    :", psutil.cpu_count(logical=True))

# CPU frequencies
# cpufq = psutil.cpu_freq()
print(f"Max Frequency                  : {cpufq.max:.2f}Mhz")
print(f"Min Frequency                  : {cpufq.min:.2f}Mhz")
print(f"Current Frequency              : {cpufq.current:.2f}Mhz")

# CPU usage
for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
    print(f"Core {i}                         : {percentage}%")

print(f"Total CPU Usage                : {psutil.cpu_percent()}%")

# Memory Information
print()
print("="*40, "Memory Information", "="*40)
# get the memory details
# mem = psutil.virtual_memory()
print(f"Total                          : {get_size(mem.total)}")
print(f"Available                      : {get_size(mem.available)}")
print(f"Used                           : {get_size(mem.used)}")
print(f"Percentage                     : {mem.percent}&")
# get the swap memory details (if exists)
print("="*40, "SWAP", "="*40)
print(f"Total                          : {get_size(swap.total)}")
print(f"Free                           : {get_size(swap.free)}")
print(f"Used                           : {get_size(swap.used)}")
print(f"Percentage                     : {get_size(swap.percent)}%")

# Disk Information
print()
print("="*40, "Disk Information", "="*40)
print("Partitions and Usage:")
# get all partitions
for part in partitions:
    print(f"=== Device: {part.device} ===")
    print(f" Mountpoint: {part.mountpoint} ")
    print(f" File system type: {part.fstype} ")

    try:
        part_usage = psutil.disk_usage(part.mountpoint)
    except PermissionError:
        continue

    print(f"    Total Size          : {get_size(part_usage.total)}")
    print(f"    Used                : {get_size(part_usage.used)}")
    print(f"    Free                : {get_size(part_usage.free)}")
    print(f"    Percentage          : {get_size(part_usage.percent)}%")


# get IO statistics since boot
print()
print("="*40, "IO statistics since boot", "="*40)
print(f"Total read                  : {get_size(disk_io.read_bytes)}")
print(f"Total writen                : {get_size(disk_io.write_bytes)}")
